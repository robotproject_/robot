/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.robotproject;

/**
 *
 * @author ASUS
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';
   
    public Robot(int x, int y, int bx, int by, int N){
       this.x = x;
       this.y = y;
       this.bx = bx;
       this.by = by;
       this.N = N;
   }
    // N, S, E, W
    // y-1, y+1, x+1, x-1
    public boolean walkToDirectionOneStep(char direction){
      switch(direction){
            case 'N':
              y = y - 1;
              break;
            case 'S':
              y = y + 1;
              break;
            case 'E':
              x = x + 1;
              break;
            case 'W':
              x = x - 1;
              break;
              
      }
        lastDirection = direction;
        return true;
  }
    public boolean walkToDirectionNStep(char direction, int step){
        for(int i=0; i<step; i++){
            if(!walkToDirectionOneStep(direction)){
                return false;
          
            }
    }
        return true;
}
    
    public boolean walkOneStep(){
        return walkToDirectionOneStep(lastDirection);
    }
    
    public boolean walkNStep(int step){
        return walkToDirectionNStep(lastDirection, step);
    }
    
    public String toString(){
        return "Robot(" + this.x + ", " + this.y + ")";
      
  }
}
